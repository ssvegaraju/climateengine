using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ClimateEngine : MonoBehaviour {

    private int year;
	private int endyear = 2200;
	private int timestep = 2;
    public TextMeshProUGUI currentYear;
    public bool useText = true;

    public TextMeshProUGUI currentOA, currentCE, currentSL, futureOA, futureCE, futureSL;
    public TextMeshProUGUI currentTemp, futureTemp;

    public GameObject currentOcean, futureOcean;
    public GameObject currentIce, futureIce;
    public Slider currentOceanPH, futureOceanPH;
    public ParticleSystem currentC02, futureCO2;
    public Slider currentTempAnomaly, futureTempAnomaly;

    private GraphConditions graph;

	// Use this for initialization
	void Start () {
		year = 2018;
        CalculateNewValues();
        graph = FindObjectOfType<GraphConditions>();
	}
	
	// Update is called once per frame
	void Update () {
        currentYear.text = "Year: " + (year);
	}

    /// <summary>
    /// Calls a specified action at year. Check EffectCalculator for available actions.
    /// </summary>
    /// <param name="method">The name of the action to execute.</param>
    public void Action(string method) { 
        EffectCalculator.InvokeMethod(method, year);
        EffectCalculator.adjustValues(year);
        CalculateNewValues();
        year += timestep;
    }

    // When the simulate button is clicked, calculate all new values.
	public void play() {
		year += timestep;
		CalculateNewValues();
	}

    /// <summary>
    /// Set the emission constant at the current year to `value_`
    /// </summary>
    /// <param name="value_">string in the form of a float/int</param>
    public void SetEmissions(System.String value_)
    {
		if (value_ != "") {
			EffectCalculator.SetEmissions (float.Parse (value_), year);
			EffectCalculator.adjustValues (year);
			CalculateNewValues ();
		}
    }

    /// <summary>
    /// Set the timestep used to calculate values between start and end years.
    /// </summary>
    /// <param name="value_">string in the form of a float/int</param>
	public void SetSensitivity(System.String value_)
	{
		if (value_ != "") {
			EffectCalculator.SetSensitivity (float.Parse (value_));
			EffectCalculator.adjustValues (year);
			CalculateNewValues ();
		}
	}

	public void SetTimestep(System.String value_) 
	{
		if (value_ != "") 
			timestep = Mathf.RoundToInt(float.Parse(value_));
	}

    /// <summary>
    /// Takes in a string year and sets that to the start year for calculations
    /// </summary>
    /// <param name="value_">string in a numerical format</param>
	public void SetYear(System.String value_)
	{
		if (value_ != "") {
			year = Mathf.RoundToInt (float.Parse (value_));
			CalculateNewValues ();
		}
	}

    // Set the new values in the scene.
    private void CalculateNewValues()
    {
        SetValues(EffectCalculator.PH(year), EffectCalculator.CO2(year),
            EffectCalculator.SeaLevel(year), EffectCalculator.Temperature(year),
			EffectCalculator.PH(endyear), EffectCalculator.CO2(endyear),
			EffectCalculator.SeaLevel(endyear), EffectCalculator.Temperature(endyear));
        if (graph != null)
            graph.SetGraphData();
    }

    // If you have text objects or visualizations set up in your scene, display values.
    private void SetValues(float currentOA, float currentCE, float currentSL, float currentTemp,
        float futureOA, float futureCE, float futureSL, float futureTemp)
    {
        if (useText) { 
            this.currentOA.text = "Current Ocean PH: " + currentOA.ToString("F2");
            this.currentCE.text = "Atmospheric Carbon: " + currentCE.ToString("F2") + " ppm";
            this.currentSL.text = "Sea Level Rise: " + currentSL.ToString("F2") + " m";
            this.currentTemp.text = "Temperature Anomaly: " + currentTemp.ToString("F1") + " C";
            this.futureOA.text = "Current Ocean PH: " + futureOA.ToString("F2");
            this.futureCE.text = "Atmospheric Carbon: " + futureCE.ToString("F2") + " ppm";
            this.futureSL.text = "Sea Level Rise: " + futureSL.ToString("F2") + " m";
            this.futureTemp.text = "Temperature Anomaly: " + futureTemp.ToString("F1") + " C";
        } else {
            currentOcean.transform.position += Vector3.up * currentSL;
            futureOcean.transform.position += Vector3.up * futureSL;

            currentIce.transform.position += Vector3.up * currentSL * 0.5f;
            futureIce.transform.position += Vector3.up * futureSL * 0.5f;

            currentOceanPH.value = currentOA;
            futureOceanPH.value = futureOA;
            currentTempAnomaly.value += currentTemp;
            futureTempAnomaly.value += futureTemp;

            currentC02.Emit(Mathf.RoundToInt(currentCE));
            futureCO2.Emit(Mathf.RoundToInt(futureCE));
            ParticleSystem.Particle[] p = new ParticleSystem.Particle[currentC02.particleCount];
            int count = currentC02.GetParticles(p);
            for(int i = 0; i < count; i++)
            {
                p[i].velocity += Vector3.up * 5;
            }
            currentC02.SetParticles(p, count);
            p = new ParticleSystem.Particle[futureCO2.particleCount];
            count = futureCO2.GetParticles(p);
            for(int i = 0; i < count; i++)
            {
                p[i].velocity += Vector3.up * 5;
            }
            futureCO2.SetParticles(p, count);
        }
    }

}
