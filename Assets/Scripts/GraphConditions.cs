using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphConditions : MonoBehaviour {

    public int startYear = 1751;
    public int endYear = 2200;
    public GameObject emptyGraphPrefab;

    WMG_Axis_Graph graph;
    WMG_Series atmosphereSeries;
    public List<Vector2> atmosphericData;
	// Use this for initialization
	void Start () {
        // Instantiate Graph Object
        GameObject g = Instantiate(emptyGraphPrefab);
        g.transform.SetParent(transform, false);
        graph = g.GetComponent<WMG_Axis_Graph>();
        atmosphereSeries = graph.addSeries();
        SetGraphData();
	}

    public void SetGraphData()
    {
        int len = Mathf.RoundToInt(EffectCalculator.Atmosphere.Length / (endYear - startYear));
        // Set Atmospheric Data
        atmosphericData.Clear();
        for (int i = 0; i < EffectCalculator.Atmosphere.Length; i += len)
        {
            atmosphericData.Add(new Vector2(i / len + startYear, EffectCalculator.Atmosphere[i]));
        }
        // Assign Data to graph
        atmosphereSeries.pointValues.Clear();
        atmosphereSeries.pointValues.SetList(atmosphericData);
        atmosphereSeries.seriesName = "Atmospheric Carbon (ppm)";
        graph.xAxis.AxisMaxValue = endYear;
        graph.xAxis.AxisMinValue = startYear;
        graph.xAxis.SetLabelsUsingMaxMin = true;
        graph.xAxis.LabelType = WMG_Axis.labelTypes.ticks;
        graph.yAxis.AxisMaxValue = Mathf.Max(EffectCalculator.Atmosphere);
    }
}
