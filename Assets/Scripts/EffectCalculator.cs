using System.IO;
using UnityEngine;
using System.Reflection;
using System;

public class EffectCalculator : MonoBehaviour {

    // All the constants used to calculate values for co2 emissions, ocean ph, sea level rise,
    // and temperature anomaly.
    #region Variables
    public static float EMISSION = 10;

    public static float[] Atmosphere
    {
        get
        {
            return atmosphere;
        }
    }

    private static float timeStep = 0.01f; // Numberical Integration time step
	private static int endyear = 2200; // End year of data
	private static int startyear = 1751; // Start year for data
	private static int INTERVAL = getIndex(endyear) + 1; // Size of data set collected
	private static float CO2PreIndustrial = 596f; // initial CO2 Levels in GtC
	private static float TempPreIndustrial = 298f; // pre industrial temperature in kelvin
	private static float gtppmconv = 2.13f; // Conversion factor between GtC and ppm Co2
	private static float ka = 0.2f; // atmosphere ocean CO2 diffusion constant
	private static float kd = 0.05f; // upper ocean and lower ocean CO2 diffusion constant
	private static float del = 50; // ratio of water mass in lower ocean compared to upper ocean
	private static float gamma = 3.1f; // atmosphere ocean heat transfer coefficient
	private static float f0 = 0.57f; // feedback mean value
	private static float sig = 0.12f; // standard deviation in feedback
	private static float lambda0 = 0.31f; // minimum climate sensitivity
	private static float muat = 0.0208f; // atmosphere temperature equilibriation rate
	private static float mulo = (1/60f); // lower ocean temperature equilibriation rate
	private static float S = 35; // ocean salinity
	private static float alk = 662.7f; // ocean alkalinity
	private static float r = 0.00226923f; // ratio of CO2 in atmosphere to ocean
	private static float lsp = 0.25f; // percent of CO2 emmissions absorbed by land bodies



	static float F, Teq, Tk, k0, kh, k1, k2, hp, M, A, B, lambda;
	static float[] atmosphere, upperOcean, lowerOcean, Tat, Tlo, sealev, ph, emissions;

    public static EffectCalculator instance;
    #endregion

    // Initialize values before any other script in Awake rather than start.
    void Awake() {
        #region Singleton
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        #endregion

		// read in emissions data from 1751 till 2011
		using(var reader = new StreamReader("Assets/emissions.csv"))
		{
			var line = reader.ReadLine ();
			emissions = new float[INTERVAL];
			while (!reader.EndOfStream) {
				line = reader.ReadLine ();
				var values = line.Split (',');
				int year_ = Mathf.RoundToInt (float.Parse (values [1]));
				float emission_ = float.Parse (values [2]) / 1000f;
				for (int i = getIndex (year_); i < getIndex (year_ + 1); i++) {
					emissions [i] = emission_;
				}
			}
		}

		// grab a random feedback value from a gaussian distribution
		float f = UnityEngine.Random.Range (-1f, 1f);
		f = Mathf.Sign (f) * Mathf.Sqrt (-Mathf.Log (1 - Mathf.Abs (f)) * 2 * Mathf.Pow (sig, 2)) + f0;
		// climate sensitivity
		lambda = lambda0 / (1 - f);

		// CO2 in specific domain
        atmosphere = new float[INTERVAL];
        upperOcean = new float[INTERVAL];
        lowerOcean = new float[INTERVAL];
		// temperature of atmosphere
		Tat = new float[INTERVAL];
		// temperature of lower ocean
		Tlo = new float[INTERVAL];
		// sea level anomoly
		sealev = new float[INTERVAL];
		// ocean ph
		ph = new float[INTERVAL];

        atmosphere[0] = 596f;
        upperOcean[0] = 591f;
        lowerOcean[0] = 29574f;
		Tat [0] = 0f;
		Tlo [0] = 0f;
		sealev [0] = 0;
		ph [0] = 8.28f;
		adjustValues (startyear);
	}

    // A helper method to return a year as a 0-based index on the interval
    // between start year and end year.
    public static int getIndex(int year_) {
        return Mathf.RoundToInt((year_ - startyear) / timeStep) + 1;
    }

    /// <summary>
    /// Calculate all values up until end year from `year` in increments of
    /// `timeStep`
    /// </summary>
    /// <param name="year_">The year to start calcaulating values from</param>
	public static void adjustValues(int year_)
    {
		for (int i = getIndex(year_); i < INTERVAL; i++) {
			
			// if there is no emissions data for the current year use last years emissions data
			if (emissions[i]==0f) 
				emissions [i] = emissions [i - 1];
			
			F = 3.8f * (Mathf.Log (atmosphere [i-1] / CO2PreIndustrial) / Mathf.Log (2));
			Teq = F*lambda;
			Tat [i] = Tat [i - 1] + muat * ((Teq - Tat [i - 1])/lambda - gamma * (Tat [i - 1] - Tlo [i - 1])) * timeStep;
			Tlo [i] = Tlo [i - 1] + mulo * gamma * (Tat [i - 1] - Tlo [i - 1]) * timeStep;
			Tk = Tat [i-1] + TempPreIndustrial;
			k0 = Mathf.Exp(9345.17f / Tk - 60.2409f + 23.3585f * Mathf.Log(Tk / 100) + 35 * Mathf.Pow(0.023517f - 0.00023656f * Tk + 0.0047036f * Tk / 100,2));
			kh = k0 * (55.57f / 1.027f) * 1000;
			k1 = Mathf.Pow(10,-(-13.721f + (0.031334f * Tk) + 3235.76f / Tk + 1.3f * (0.00001f) * S * Tk - (0.1031f * Mathf.Sqrt(S))));
			k2 = Mathf.Pow(10, - (5371.96f + 1.671221f*Tk + 0.22913f*S + 18.3802f*Mathf.Log10(S) - 128375.28f/Tk - 2194.30f*Mathf.Log10(Tk) - 8.0944f*(0.0001f)*S*Tk - 5617.11f*Mathf.Log10(S)/Tk + 2.136f*S/Tk));
			B = 1/(1 + k1 /(Mathf.Pow(10,-ph[i-1])) + k1 * k2 / Mathf.Pow(10,-2*ph[i-1]));
			A = kh * r * (del+1);
			M = upperOcean [i-1] / alk;
			hp = 0.5f* (-k1 + k1 * M + Mathf.Sqrt (k1) * Mathf.Sqrt (k1 - 4 * k2 - 2 * k1 * M + 8 * k2 * M + k1 * Mathf.Pow(M,2)));
			ph[i] = - Mathf.Log10(hp);
			sealev [i] = sealev [i - 1] + (0.0031f) * (Tat [i-1])*timeStep;
			atmosphere [i] = atmosphere [i - 1] + (emissions[i]*(1-lsp) - ka * atmosphere [i - 1] + ka * A*B * upperOcean [i - 1]) * timeStep;
			upperOcean [i] = upperOcean [i - 1] + (ka * atmosphere [i - 1] - (ka * A*B + kd) * upperOcean [i - 1] + (kd / del) * lowerOcean [i - 1]) * timeStep;
			lowerOcean [i] = lowerOcean [i - 1] + (kd * upperOcean [i - 1] - (kd / del) * lowerOcean [i - 1]) * timeStep;
		}
    }

    /// <summary>
    /// Get the total amount of carbon in the atmosphere at 'year' (parts-per-million)
    /// </summary>
    /// <param name="year_">The year at which to request carbon levels.</param>
    /// <returns></returns>
	public static float CO2(int year_)
    {
        return atmosphere[getIndex(year_)] / gtppmconv;
    }

    /// <summary>
    /// returns the temperature anomaly from starting year to year.
    /// </summary>
    /// <param name="year_">The year at which to request temperature anomaly</param>
    /// <returns></returns>
    public static float Temperature(int year_)
    {
        return Tat[getIndex(year_)];
    }

    /// <summary>
    /// Get the ocean acidification level at year
    /// </summary>
    /// <param name="year_">The year at which to request ocean acidification levels</param>
    /// <returns></returns>
	public static float PH(int year_)
    {
        return ph[getIndex(year_)];
    }

    /// <summary>
    /// Get the Sea Level Rise at year since the starting year.
    /// </summary>
    /// <param name="year_">The year at which to request sea level rise.</param>
    /// <returns></returns>
	public static float SeaLevel(int year_)
    {
        return sealev[getIndex(year_)];
    }

    /// <summary>
    /// Set the constant emission rate.
    /// </summary>
    /// <param name="emission_"></param>
	public static void SetEmissions(float emission_, int year_)
    {
//		print (emission_.GetType());
        for (int i = getIndex(year_); i < emissions.Length; i++)
		    emissions[i] = emission_;
    }


	public static void SetSensitivity(float s_) {
		lambda = s_;
	}

    /// <summary>
    /// reduce emissions by dE_.
    /// </summary>
    /// <param name="dE_">Amount of Carbon by which to reduce emissions (gigatons)</param>
	public static void reduceEmissions(float dE_, int year_)
	{
		emissions[getIndex(year_)] = emissions[getIndex(year_)] - dE_;
	}

    /// <summary>
    /// Simulate geoengineering a cloud cover.
    /// </summary>
    /// <param name="dL_">Amount of change to the cloud cover.</param>
	public static void Geoengineer(float dL_)
	{
		lambda = lambda + dL_;
	}


    /// <summary>
    /// Removes dC_ of carbon from the atmosphere
    /// </summary>
    /// <param name="dC_">Amount of carbon to remove.</param>
    /// <param name="year_">Year from which to remove carbon.</param>
	public static void scrubCO2(float dC_, float year_)
    {
        atmosphere[getIndex((int)year_)] = atmosphere[getIndex((int)year_)] - dC_;
    }

    /// <summary>
    /// calls a certain action on the climate system.
    /// </summary>
    /// <param name="methodName_">The name of the action to invoke.</param>
    /// <param name="year_">The year at which to invoke the action.</param>
    public static void InvokeMethod(string methodName_, float year_)
    {
        Type type = typeof(EffectCalculator);
        MethodInfo method = type.GetMethod(methodName_);
        switch (methodName_)
        {
            case "scrubCO2":
                method.Invoke(instance, new object[] { 3f * 2, year_ });
                break;
            case "Geoengineer":
                method.Invoke(instance, new object[] { 0.01f * 2});
                break;
            case "reduceEmissions":
                method.Invoke(instance, new object[] { 0.08f * 2});
                break;
            default:
                Debug.LogError("Method does not exist. Check spelling?");
                break;
        }
    }
}
    
